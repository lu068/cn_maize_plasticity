# CN_MAIZE_PLASTICITY



This model requires the simulation platform GroIMP (https://gitlab.com/grogra/groimp). After downloading and installing GroIMP, download the code or clone it using git. Start GroIMP and open the project.gs file in the model folder.


Parameter.rgg is a generic model parameter file and Parameter9.rgg is the maize parameter file. To run the model automatically in one day time steps till the harvest day, please click “run run”. To run the model for a single day, please click “run”, this can then be repeated for each consecutive day. 


After a model run has finished, the outputs will be stored as .txt in the location defined in the “pathData” parameter. Alternatively, the datasets can be exported at any moment during the simulation from “Datasets”. 


For further use of the GroIMP software, please refer to www.grogra.de.
